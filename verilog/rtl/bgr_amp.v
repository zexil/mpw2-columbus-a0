// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

`default_nettype none
/*
 *-------------------------------------------------------------
 *
 * Bandgap Refrence
 *
 *-------------------------------------------------------------
 */

module bgr_amp
    (
    `ifdef USE_POWER_PINS
        inout vdd,
        inout vss,
    `endif
        inout vip,
        inout vin,
        inout vo,
        inout vcomp,
        inout ir50u
    );

    `ifdef USE_POWER_PINS
    assign vo    = vdd & ~vss & ir50u &  (vip ^ vin);
    assign vcomp = vdd & ~vss & ir50u & ~(vip ^ vin);
    `else
    assign vo    = ir50u &  (vip ^ vin);
    assign vcomp = ir50u & ~(vip ^ vin);
    `endif
endmodule

`default_nettype wire
